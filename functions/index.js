const firebase = require("firebase");
const FCM = require('fcm-node');
const serverKey = 'AAAAWreZoXo:APA91bHM_q9FRTLGlDmwIWVAtVzvEdQ0LwdIPT9Qy5g1MStwaT-lFOXw1HSXrfMLhpYd6ulhb_kspbZ254AMvmgkMw8iEltpNC8HrxoXQAesF0_qF7PMwAonzbINf-ghJFO1yqkd2EsH';
const fcm = new FCM(serverKey);
require("firebase/firestore");

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const algoliasearch = require('algoliasearch');

const gcs = require('@google-cloud/storage')();
const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');

const ALGOLIA_ID = "WEJRVSUTUH";
const ALGOLIA_ADMIN_KEY = "aed2eeef2fded98a574c9ed4303794ca";
const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

const moment = require('moment');

admin.initializeApp(functions.config().firebase);

var config = {
  apiKey: "AIzaSyDrWmCr8XUmxaCD0LhFeW7Rf0hJl9qsLWY",
  authDomain: "globalist-1246f.firebaseapp.com",
  databaseURL: "https://globalist-1246f.firebaseio.com",
  projectId: "globalist-1246f",
  storageBucket: "globalist-1246f.appspot.com",
  messagingSenderId: "389627355514"
};
firebase.initializeApp(config);

exports.onItineraryCreated = functions.firestore.document("itineraries/{itineraryId}").onCreate((event, context) => {
    // Get the note document
    const itinerary = event.data();
  
    // Add an "objectID" field which Algolia requires
    itinerary.objectID = context.params.itineraryId;

    //Set Date
    itinerary.unixTimestamp = moment(itinerary.updatedAt.toDate()).unix();
    itinerary.createdAt = moment(itinerary.createdAt.toDate()).unix();
    itinerary.updatedAt = moment(new Date()).unix();

    // Write to the algolia index
    const index = client.initIndex("itineraries");
    index.setSettings({
      attributesForFaceting: [
        'owner'
      ],
      ranking: [
        "desc(unixTimestamp)"
      ]
    });
    return index.saveObject(itinerary);
  });

exports.onItineraryUpdated = functions.firestore.document("itineraries/{itineraryId}").onUpdate((event, context) => {
  // Get the note document
  const itinerary = event.after.data();

  // Add an "objectID" field which Algolia requires
  itinerary.objectID = context.params.itineraryId;

  //Set Date
  itinerary.unixTimestamp = moment(itinerary.updatedAt.toDate()).unix();
  itinerary.createdAt = moment(itinerary.createdAt.toDate()).unix();
  itinerary.updatedAt = moment(new Date()).unix();

  // Write to the algolia index
  const index = client.initIndex("itineraries");
  index.setSettings({
    attributesForFaceting: [
      'owner'
    ],
    ranking: [
      "desc(unixTimestamp)"
    ]
  });
  return index.saveObject(itinerary);
});

exports.onItineraryDeleted = functions.firestore.document("itineraries/{itineraryId}").onDelete((event, context) => {

  // Add an "objectID" field which Algolia requires
  const id = context.params.itineraryId;

  // Delete on the algolia index
  const index = client.initIndex("itineraries");
  return index.deleteObject(id, function(err) {
    if (err) {
      console.log('Object was not deleted');
      console.log(err)
    }
  });
});

exports.onPlaceCreated = functions.firestore.document("places/{placeId}").onCreate(event => {
  // Get the note document
  const place = event.data();

  // Add an "objectID" field which Algolia requires
  place.objectID = context.params.placeId;

  //Set Date
  place.unixTimestamp = moment(place.updatedAt.toDate()).unix();
  place.createdAt = moment(place.createdAt.toDate()).unix();
  place.updatedAt = moment(new Date()).unix();

  // Write to the algolia index
  const index = client.initIndex("places");
  index.setSettings({
    attributesForFaceting: [
      'owner',
      'id',
      'city'
    ],
    ranking: [
      "desc(unixTimestamp)"
    ]
  });
  return index.saveObject(place);
});

exports.onPlaceUpdated = functions.firestore.document("places/{placeId}").onUpdate((event, context) => {
  // Get the note document
  const place = event.after.data();

  // Add an "objectID" field which Algolia requires
  place.objectID = context.params.placeId;

  //Set Date
  let now = new Date()
  place.unixTimestamp = moment(now).unix();
  place.updatedAt = moment(now).unix();

  // Write to the algolia index
  const index = client.initIndex("places");
  index.setSettings({
    attributesForFaceting: [
      'owner',
      'id',
      'city'
    ],
    ranking: [
      "desc(unixTimestamp)"
    ]
  });
  return index.saveObject(place);
});

exports.onPlaceDeleted = functions.firestore.document("places/{placeId}").onDelete((event, context) => {

  // Add an "objectID" field which Algolia requires
  const id = context.params.placeId;

  // Delete on the algolia index
  const index = client.initIndex("places");
  return index.deleteObject(id, function(err) {
    if (err) {
      console.log('Object was not deleted');
      console.log(err)
    }
  });
});

exports.onEntryCreated = functions.firestore.document("places/{placeId}/entries/{entryId}").onCreate((event, context) => {
  // Get the note document
  const entry = event.data();

  // Add an "objectID" field which Algolia requires
  entry.objectID = context.params.entryId;
  entry.placeId = context.params.placeId;

  //Set Date
  entry.unixTimestamp = moment(entry.updatedAt.toDate()).unix();
  entry.createdAt = moment(entry.createdAt.toDate()).unix();
  entry.updatedAt = moment(new Date()).unix();

  // Write to the algolia index
  const index = client.initIndex("entries");
  index.setSettings({
    attributesForFaceting: [
      'owner',
      'placeId'
    ],
    ranking: [
      "desc(unixTimestamp)"
    ]
  });
  return index.saveObject(entry);
});

exports.onEntryUpdated = functions.firestore.document("places/{placeId}/entries/{entryId}").onUpdate((event, context) => {
  // Get the note document
  const entry = event.after.data();

  //Set Date
  let now = new Date()
  entry.unixTimestamp = moment(now).unix();
  entry.updatedAt = moment(now).unix();

  // Add an "objectID" field which Algolia requires
  entry.objectID = context.params.entryId;
  entry.placeId = context.params.placeId;
  
  // Write to the algolia index
  const index = client.initIndex("entries");
  index.setSettings({
    attributesForFaceting: [
      'owner',
      'placeId'
    ],
    ranking: [
      "desc(unixTimestamp)"
    ]
  });
  return index.saveObject(entry);
});

exports.onEntryDeleted = functions.firestore.document("places/{placeId}/entries/{entryId}").onDelete((event, context) => {

  // Add an "objectID" field which Algolia requires
  const id = context.params.entryId;

  // Delete on the algolia index
  const index = client.initIndex("entries");
  return index.deleteObject(id, function(err) {
    if (err) {
      console.log('Object was not deleted');
      console.log(err)
    }
  });
});

exports.onUserCreated = functions.firestore.document("users/{id}").onCreate((event, context) => {
  // Get the note document
  const user = event.data();

  // Add an "objectID" field which Algolia requires
  user.objectID = context.params.id;
  user.createdAt = moment(user.createdAt.toDate()).unix();
  user.updatedAt = moment(new Date()).unix();

  // Write to the algolia index
  const index = client.initIndex("users");
  return index.saveObject(user);
});

exports.onUserUpdated = functions.firestore.document("users/{id}").onUpdate((event, context) => {
  // Get the note document
  const user = event.after.data();

  // Add an "objectID" field which Algolia requires
  user.objectID = context.params.id;
  user.updatedAt = moment(new Date()).unix();

  // Write to the algolia index
  const index = client.initIndex("users");
  return index.saveObject(user);
});

exports.onUserDeleted = functions.firestore.document("users/{id}").onDelete((event, context) => {

  // Add an "objectID" field which Algolia requires
  const id = context.params.id;

  // Write to the algolia index
  const index = client.initIndex("users");
  return index.deleteObject(id, function(err) {
    if (err) {
      console.log('Object was not deleted');
      console.log(err)
    }
  });
});

exports.onNotificationCreated = functions.firestore.document("users/{userId}/notifications/{id}").onCreate((event, context) => {

  const notification = event.data();

  var db = firebase.firestore();

  // Get a reference to the place
  var userRef = db.collection('users').doc(context.params.userId);

  var getDoc = userRef.get()
    .then(doc => {
        if (!doc.exists) {
            console.log('No such document!');
        } else {
            const user = doc.data();
            var fcmToken = user.fcmToken;
            console.log("SENDING NOTIF TO TOKEN: " + fcmToken);

            var message = { 
              to: fcmToken, 
              collapse_key: 'your_collapse_key',
              
              notification: { 
                  body: notification.firstName + " " + notification.description
              },
              
              data: {
                  my_key: 'my value',
                  my_another_key: 'my another value'
              }
            };

            return fcm.send(message, function(err, response){
              if (err) {
                  console.log("Something has gone wrong with FCM message: ", err);
              } else {
                  console.log("Successfully sent with response: ", response);
              }
            });
        }
    })
    .catch(err => {
        console.log('Error getting document', err);
        return 0
    });

});

//STORAGE
exports.generateThumbnail = functions.storage.object().onFinalize((object) => {

  const fileBucket = object.bucket; // The Storage bucket that contains the file.
  const filePath = object.name; // File path in the bucket.
  const contentType = object.contentType; // File content type.
  const resourceState = object.resourceState; // The resourceState is 'exists' or 'not_exists' (for file/folder deletions).
  const metageneration = object.metageneration; // Number of times metadata has been generated. New objects have a value of 1.
  // [END eventAttributes]

  // [START stopConditions]
  // Exit if this is triggered on a file that is not an image.
  if (!contentType.startsWith('image/')) {
    console.log('This is not an image.');
    return null;
  }

  // Get the file name.
  const fileName = path.basename(filePath);
  // Exit if the image is already a thumbnail.
  if (fileName.startsWith('thumb_')) {
    console.log('Already a Thumbnail.');
    return null;
  }

  // Exit if this is a move or deletion event.
  if (resourceState === 'not_exists') {
    console.log('This is a deletion event.');
    return null;
  }

  // Exit if file exists but is not new and is only being triggered
  // because of a metadata change.
  if (resourceState === 'exists' && metageneration > 1) {
    console.log('This is a metadata change event.');
    return null;
  }
  // [END stopConditions]

  // [START thumbnailGeneration]
  // Download file from bucket.
  const bucket = gcs.bucket(fileBucket);
  const tempFilePath = path.join(os.tmpdir(), fileName);
  const metadata = {
    contentType: contentType,
  };
  return bucket.file(filePath).download({
    destination: tempFilePath,
  }).then(() => {
    console.log('Image downloaded locally to', tempFilePath);
    // Generate a thumbnail using ImageMagick.
    return spawn('convert', [tempFilePath, '-resize', '600', tempFilePath], {capture: ['stdout', 'stderr']});
  }).then(() => {
    console.log('Thumbnail created at', tempFilePath);
    // We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
    const thumbFileName = `thumb_${fileName}`;
    const thumbFilePath = path.join(path.dirname(filePath), thumbFileName);
    // Uploading the thumbnail.
    return bucket.upload(tempFilePath, {
      destination: thumbFilePath,
      metadata: metadata,
    });
    // Once the thumbnail has been uploaded delete the local file to free up disk space.
  }).then(() => fs.unlinkSync(tempFilePath));
  // [END thumbnailGeneration]
});